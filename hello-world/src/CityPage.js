import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-refetch';
import LinearProgress from 'material-ui/LinearProgress';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


import ForecastInfo from './ForecastInfo';


const _APIKEY_ = '31a3eee569a64e44b95150731170506';

class LoadingAnimation extends Component {

    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <LinearProgress />
                </div>
            </MuiThemeProvider>
        );
    }
}

class Error extends Component {
    render() {
        return (
            <div>
                <div>I can't find any forecasts for "{this.props.cityName}" city</div>
                <Link to="/">Get forecast for another city</Link>
            </div>
        );
    }
}




const CityPage = function (data) {
    const dataFetch = data.cityFetch;
    const cityName = data.match.params.cityName;
    const forecastTerm = data.match.params.forecastTerm;
    if (dataFetch.pending) {
        return <LoadingAnimation/>
    } else if (dataFetch.rejected) {
        return <Error error={dataFetch.reason} cityName={cityName}/>
    } else if (dataFetch.fulfilled) {
        return <ForecastInfo
            cityInfo={dataFetch.value}
            cityName={cityName}
            forecastTerm={forecastTerm}
             />
    }
};

export default connect((props) => ({
    cityFetch: `http://api.apixu.com/v1/forecast.json?key=${_APIKEY_}&q=${props.match.params.cityName}&days=${props.match.params.forecastTerm}`
}))(CityPage)
