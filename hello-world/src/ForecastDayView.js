import React from 'react';
import './ForecastDayView.css';

import {
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

const style = {
    textAlign: 'center'
};


const ForecastDay = function (props) {
    const {iconSrc, iconText, temp_c, time} = props;
    return (
        <TableRow>
            <TableRowColumn style={style}>{time.split(' ').pop()}</TableRowColumn>
            <TableRowColumn style={style}>{temp_c} °C</TableRowColumn>
            <TableRowColumn style={style}>
                <img src={iconSrc} alt={iconText}/>
                <div>{iconText}</div>
            </TableRowColumn>
        </TableRow>
    );
};

export default ForecastDay;