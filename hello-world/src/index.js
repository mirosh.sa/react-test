import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import CityPage from './CityPage';

import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={App}/>
            <Route path="/:forecastTerm/:cityName" component={CityPage}/>
        </div>
    </Router>,
    document.getElementById('root')
);


registerServiceWorker();
