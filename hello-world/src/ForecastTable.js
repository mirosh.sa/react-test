import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import  ForecastDay from './ForecastDayView';
import './ForecastInfo.css';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow
} from 'material-ui/Table';

import './ForecastInfo.css';

const tableStyle = {
    textAlign: 'center'
};


const ForecastTable = function (props) {
    const {forecast} = props;
    console.log(props)
    return (
        <div>
            <h2 style={{textAlign:'center'}}>Forecast for {forecast.date}</h2>
            <MuiThemeProvider>
                <Table>
                    <TableHeader className="tableHeader">
                        <TableRow>
                            <TableHeaderColumn style={tableStyle}>Time</TableHeaderColumn>
                            <TableHeaderColumn style={tableStyle}>Temperature</TableHeaderColumn>
                            <TableHeaderColumn style={tableStyle}>Condition</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {
                            forecast.hour.map(hour =>
                                <ForecastDay
                                    key={hour.time_epoch}
                                    iconSrc={hour.condition.icon}
                                    iconText={hour.condition.text}
                                    temp_c={hour.temp_c}
                                    time={hour.time}
                                />
                            )
                        }
                    </TableBody>
                </Table>
            </MuiThemeProvider>
        </div>
    );
};

export default ForecastTable;