import React, {Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {withRouter} from 'react-router';
import TextField from 'material-ui/TextField';

class MainPage extends Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }


    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSearch() {
        console.log(this.props)
        if (this.state.value) {
            console.log(this.state.value);
            console.log(this.props)

            this.props.history.push(`1/${this.state.value}`);
        }
    }

    render() {
        return (
            <div className="weatherForecast">
                <div className="searchFieldHolder">
                    <h1>Check the weather in your city</h1>
                    <TextField
                        id="text-field-controlled"
                        hintText="Your city name"
                        onChange={this.handleChange}
                    />

                    <div >
                        <RaisedButton onClick={this.handleSearch} label="Get your city weather forecast"/>
                    </div>

                </div>
            </div>
        );
    }
}


export default withRouter(MainPage);